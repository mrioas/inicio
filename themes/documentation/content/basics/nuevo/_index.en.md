---
title: Links
weight: 5
#pre:"<b>- </b>"
chapter: true
---
![](Pictures/100002010000079F000008DA5C8DDDDE5F8AA36D.png){width="0.948in"
height="1.102in"}

[]{#anchor}Science and Technology

[*VAGRANT*](https://github.com/hashicorp/vagrant)

*creando máquina virtual con Vagrant*

"

Vagrant es una herramienta para construir y distribuir entornos de
desarrollo, puede correr sobre una plataforma virtualizada tal como
VirtualBox o VMWARE, en la nube vía AWS o OpenStack, o un container tal
como Docker o raw LXC.

Descripción

"

Las dependencias de Vagrant en un sistema Linux son las siguientes:

1.  

Vamos a crear una máquina virtual con
[*vagrant*](https://github.com/hashicorp/vagrant) y el entorno de
virtualización [*virtualbox*](https://www.virtualbox.org/), como sistema
operativo anfitrión se usará
[*ubuntu*](http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/)
en la versión base o también renombrada como la versión netbook, aquí
con los pasos explicativos con
fotos..![](Pictures/100000000000010E00000105993A9DB8AAC57B99.png){width="2.4535in"
height="2.3752in"}

-   Se procede a la instalación, aquí el primer menú del proceso, donde
    nos indica cuatro alternativas
-   **Install.-** el proceso normal de instalación y de manera gráfica.
-   **Command-line install.-** El proceso de instalación de en CLI
    (Interfaz de línea de comando).
-   **Advanced Options.- **Como la palabra traducida al español indica,
    opciones avanzadas del proceso de instalación.

![](Pictures/100000000000018B0000001C5B7B21AF3DD07E7F.png){width="4.1146in"
height="0.2917in"}

-   **Selección del Lenguaje.-** el paso de elección del lenguaje para
    la instalación, y el que será predeterminado para el sistema
    operativo. También si se da el caso elección por códigos UTF o ASCII
-   **Selección de la zona,** país, territorio o área
-   **Configuración** del Teclado, o detección automática de la
    disposición del teclado
-   Configuración y detección de la red, o configuración y detección
    automática.
-   En sucesión de la **configuración de red, **configurar el nombre del
    host o hostname para el sistema, esta configuración podrá ser
    cambiada en un posterior momento.

![](Pictures/10000000000002CF00000056CFF1A6129C8B3838.png){width="6.5in"
height="0.778in"}

-   Elegir la ubicación del servidor principal, donde están almacenados
    los paquetes, también denominado como "**Mirror of the Ubuntu
    archive**"
-   En la configuración de la máquina virtual es muy necesario tener en
    cuenta, dos tipos de redes la con dos configuraciones distintas, la
    primera configurada en **NAT **y la segunda en **Bridge**
-   Pasa que en algunos casos se puede omitir, y es en el caso de
    utilizar un servidor proxy, en caso de no utilizarlo se deja en
    blanco
-   Una vez establecida las configuraciones con los pasos previos, se
    acepta y se descargan los paquetes necesarios, es necesario tener
    internet en la primer interfaz **NAT**
-   Estableciendo y configurando el nombre de usuario **Root **y la
    contraseña "**Password" **, el ejemplo de esta instancia es Login:
    **server **y** **Password**: server**
-   **Detección **de discos disponibles (**SCSI: Small Computer System
    Interface**), y establecer el tamaño de la partición, Example: 500GB
    ATA(Advanced Technology Attachment) VBOX HARDDISK, el tipo de
    archivo "EXT 4" en la raíz "/"
-   Establecidos los pasos previos, se procederá al proceso de
    instalación, el tiempo de la instalación dependerá de las
    capacidades de la máquina virtual.
-   En el proceso de instalación se llega a una sección de instalación
    de paquetes extras, dependiendo de la necesidad a instalar, como es
    nuestro caso para instalar wordpress en el entorno de virtualización
    virtual, sobre vagrant:

    -   Hasta la fecha 2019 la versión de wordpress tiene los siguiente
        > requerimientos:

        i.  [*PHP*](https://secure.php.net/) version 7.3 or greater.
        ii. [*MySQL*](https://www.mysql.com/) version 5.6 or greater
            *OR*[
            ](https://mariadb.org/)[*MariaDB*](https://mariadb.org/)
            version 10.1 or greater.
        iii. [*HTTPS*](https://wordpress.org/news/2016/12/moving-toward-ssl/)
            support
        iv. [*Apache*](https://httpd.apache.org/) or[
            ](https://nginx.org/)[*Nginx*](https://nginx.org/)

-   En los paquetes necesarios disponibles en el proceso de instalación:

    -   **Software Bundle LAMP (A**pache, **M**ySQL,
        > **P**HP/**P**erl/**P**ython)

    -   OpenSSH Server

    -   Basic Ubuntu Server

    -   DNS Server

-   Una vez terminada la instalación, procedemos a acceder al sistema,
    con las credenciales anteriormente creadas, Vamos con los comandos
    después de una instalación limpia

    -   **server\@server:\~\$ ifconfig** → Verificamos las interfaces
        > disponibles y que se están usando desde el inicio.

    -   **server\@server:\~\$ ip a** → Verifica las interfaces de
        > Hardware disponibles, las que están y no activas

    -   En caso de querer activar una interfaz de red, como es nuestro
        > caso, haremos uso del comando **server\@server:\~\$ sudo
        > ifconfig nic up** → **nic** = Nombre de la Interfaz, pero en
        > la versión de ubuntu 18.04, este comando es de manera temporal
        > la activación de la NIC

    -   Para activar la NIC de manera permanente, debemos usar el
        > comando **NETPLAN:**

> **server\@server:\~\$ sudo nano /etc/netplan/01-netcfg.yaml **

> dentro del archivo: network:

  ----------------------------------
  \* version: 2\
  renderer: networkd\
  ethernets:\
  enp0s3:\
  dhcp4: yes\
  enp0s8:\
  addresses: \[192.168.1.105/24\]\
  gateway4: 192.168.1.1\
  nameservers:\
  search: \[server.local\]\
  addresses: \[192.168.1.1\]*

  ----------------------------------

> **server\@server:\~\$ sudo netplan apply**

-   Procedemos a la configuración de **vagrant.**

    -   La necesidad de tener dos NIC ó adaptadores de red en la máquina
        > virtual, es por que en vagrant una NIC

-   Algo a tener en cuenta es la administración de usuarios, en nuestro
    caso crearemos uno en el momento de la instalación "**server**" ,
    más el usuario "**root", **y se creará el usuario** "vagrant", **Los
    comandos a seguir son los siguientes:

\# useradd vagrant → Creamos el usuario UNIX y la contraseña

\# visudo → Nos abrirá un archivo en donde podemos modificar las
características con respecto a los permisos, agregamos el usuario
vagrant de la siguiente manera:

vagrant ALL=(ALL) NOPASSWD:ALL → Nos indica que vagrant ya pertenece al
grupo **sudo **y no requiere de contraseña para acceder.

-   Procedemos a la** configuraciones SSH** con los siguiente pasos a
    seguir:

    -   Crear la carpeta .ssh con mkdir .ssh en el directorio principal
        > de vagrant \~/

    -   Agregar las claves de emparejamiento para conectar con la
        > máquina virtual, wget
        > [*https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub
        > -O
        > .ssh/authorized\_keys*](https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub%20-O%20.ssh/authorized_keys)

    -   Debemos de tomar en cuenta que OpenSSH en bastante estricto con
        > los permisos en los archivos, dichos permisos se los puede
        > determinar de la siguiente manera:

        i.  chmod 700 .ssh
        ii. chmod 600 .ssh/authorized\_keys
        iii. chown -R vagrant:vagrant .ssh

-   -   Un paso para mejorar la velocidad de la conexiones SSH a la
        > máquina virtual es modificando el archivo sshd\_config ubicada
        > en /etc/ssh, agregando UseDNS no y por último reiniciando el
        > servicio service ssh restart o systemctl restart ssh

-   **Instalando VirtualBox Guest Additions**, descargar la ISO
    difiriendo sólo de la versión de VirtualBox que tienes instalados en
    este caso la versión
    [*6.0.14*](https://download.virtualbox.org/virtualbox/6.0.14/VBoxGuestAdditions_6.0.14.iso)
    , para que vagrant comparta correctamente los folders entre el
    sistema operativo Anfitrión (**Host**) y Huésped (**Guest**), los
    pre requisitos necesarios para instalar VirtualBox Guest Additions
    en linux son: Linux header y herramientas de desarrollo.

  --------------------------------------------------------------------
  sudo apt-get install linux-headers-generic build-essential dkms -y
  --------------------------------------------------------------------

-   **Adicionar** Settings\>Storage\>Storage Devices\>Add IDE (Optical
    Drive)
-   **Montamos y corremos** el script en la ISO y reiniciamos

+-------------------------------------+
| sudo mount /dev/cdrom /media/cdrom\ |
| cd /media/cdrom\                    |
| sudo ./VBoxLinuxAdditions.run       |
|                                     |
| sudo reboot                         |
+-------------------------------------+

-   **Creando el paquete base Vagrant**

    -   i.  Empaquetando el Basebox ➜ \~vagrant package *\--base
            \<NameVirtualMachine\>*

![](Pictures/100000000000015600000020D59E0549359F28ED.png){width="3.5626in"
height="0.3335in"}

-   -   i.  Para identificar el nombre la máquina virtual ➜ \~
            VBoxManage list vms
        ii. Por tema de respaldo de igual manera podemos crear un
            archivo **OVA(Open Virtual Appliance),** que es de fácil
            creación directamente desde la GUI de VirtualBox, cómo
            también desde el CLI **VBoxManage, **explicaremos desde la
            GUI: **File\>Export Appliance\> Choose the VM and Settings\>
            Export .ova**
        iii. El paso siguiente es agregar el paquete base creado
            anteriormente a la lista de Vagrant: ➜ vagrant box add
            \<NameofBox\>.box *\--name NameInstance*
        iv. El archivo de configuración, es un archivo sin extensión
            dentro de él tiene la información necesaria, para iniciar
            una Máquina Virtual con los parámetros y/o configuraciones
            necesarias, aquí un ejemplo básico:

> ***Vagrant.configure(\"2\") do \|config\|***

> *** config.vm.box = \"Wordpress\"***

> *** config.vm.network \"public\_network\"***

> *** config.vm.provision \"shell\", inline: \<\<-SHELL***

> *** apt-get update***

> *** apt-get install -y zsh***

> *** SHELL***

> ***end***

-   -   i.  Una vez creado el paquete básico, seguir los siguientes
            comandos:

            -   ***➜ vagrant init,*** creará el archivo base***
                > Vagrantfile***

            -   ***➜ vagrant up,*** Iniciar la instancia***
                > VirtualBox*** con*** Vagrant ***

            -   ***➜ vagrant halt,*** Parar la instancia

            -   ***➜ VBoxManage unregistervm \--delete \<VM Name\>,***
                > En caso de error se debe crear una nueva instancia,
                > pero eliminando los rastros de la anterior.

            -   ***➜ vagrant ssh***, acceder vía ssh a la instancia
                > virtual.

-   **Procediendo a la instalación de Wordpress**

    -   **gg**

    -   **freg**

    -   **gre**

    -   **hytj**

    -   **ghf**

    -   Una opciones más buscadas recurrentemente en WordPress, es el
        > cambio de lenguaje, el cual aprenderemos a realizarlo

"

Referencias

"

[*https://mytcpip.com/2019/05/05/ubuntu-18-04-y-netplan-tutorial-rapido-de-los-cambios-de-networking/*](https://mytcpip.com/2019/05/05/ubuntu-18-04-y-netplan-tutorial-rapido-de-los-cambios-de-networking/)

[*https://www.vagrantup.com/docs/virtualbox/boxes.html*](https://www.vagrantup.com/docs/virtualbox/boxes.html)

[*https://www.sitepoint.com/create-share-vagrant-base-box/*](https://www.sitepoint.com/create-share-vagrant-base-box/)

[*https://wordpress.org/about/requirements/*](https://wordpress.org/about/requirements/)

[*https://www.vagrantup.com/docs/networking/public\_network.html*](https://www.vagrantup.com/docs/networking/public_network.html)

[*https://linuxize.com/post/how-to-install-wordpress-with-apache-on-ubuntu-18-04/*](https://linuxize.com/post/how-to-install-wordpress-with-apache-on-ubuntu-18-04/)
