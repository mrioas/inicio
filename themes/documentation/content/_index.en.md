---
title : Inicio 
weight : 15
---
## Descripción

Este es el inicio descriptivo de toda la documentación creada, para compartir documentos de propia autoria, o investigación con referencias enlazadas.


Mi nombre es `Mario Aguilar Salazar`, con el lapso del tiempo compartiré más información acerca de mi.

```
hugo new site <new_project>
```

{{% notice note %}}
This website 
{{% /notice %}}
